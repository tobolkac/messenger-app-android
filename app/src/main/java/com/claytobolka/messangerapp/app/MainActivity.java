package com.claytobolka.messangerapp.app;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.parse.Parse;


public class MainActivity extends Activity implements MessageListFragment.onMessageListFragmentListener, ConversationFragment.onConversationFragmentListener{

    private FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Parse.initialize(this, "PDBifLK5rHUHMgDdxVDNTSQp9b45KXNgjuCReTwq", "vYKMKwz9AD5IDFjl7pFXnVYpZLggtWYmpV7uObSR");


        manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragmentFrame, MessageListFragment.newInstance()); // newInstance() is a static factory method.
        transaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMessageListFragmentInteraction(int id) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragmentFrame, ConversationFragment.newInstance(id)).addToBackStack(null); // newInstance() is a static factory method.
        transaction.commit();
        Toast.makeText(getApplicationContext(), "Clicked Message "+String.valueOf(id), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConversationFragmentInteraction(int id) {

    }
}
